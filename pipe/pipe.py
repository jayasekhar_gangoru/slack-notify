import json
import os
import pathlib

import requests
from bitbucket_pipes_toolkit import Pipe, yaml, get_variable


# global variables
REQUESTS_DEFAULT_TIMEOUT = 10
BASE_SUCCESS_MESSAGE = "Notification successful"
BASE_FAILED_MESSAGE = "Notification failed"


# defines the schema for pipe variables
schema = {
    "WEBHOOK_URL": {
        "type": "string",
        "required": True
    },
    "DEBUG": {
        "type": "boolean",
        "default": False
    }
}

attachments_schema = {
    "MESSAGE": {
        "type": "string",
        "required": True
    },
    "PRETEXT": {
        "type": "string",
        "required": False
    }
}

payload_file_schema = {
    "PAYLOAD_FILE": {
        "type": "string",
        "required": True
    }
}


class SlackNotifyPipe(Pipe):

    def notify(self):
        webhook_url = self.get_variable("WEBHOOK_URL")
        message = self.get_variable("MESSAGE")
        pretext = self.get_variable("PRETEXT")
        payload_file_path = self.get_variable("PAYLOAD_FILE")
        debug = self.get_variable("DEBUG")

        # get pipelines specific variables
        workspace = get_variable('BITBUCKET_WORKSPACE', default='local')
        repo = get_variable('BITBUCKET_REPO_SLUG', default='local')
        build = get_variable('BITBUCKET_BUILD_NUMBER', default='local')

        if debug:
            self.log_info("Enabling debug mode.")
        self.log_info("Sending notification to Slack...")

        headers = {'Content-Type': 'application/json'}

        if not pretext:
            pretext = f"Notification sent from <https://bitbucket.org/{workspace}/{repo}/addon/pipelines/home#!/results/{build}|Pipeline #{build}>"

        payload = {
            "blocks": [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": pretext
                    }
                },
                {
                    "type": "divider"
                },
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": message
                    }
                }
            ]
        }

        if payload_file_path:
            if not pathlib.Path(payload_file_path).exists():
                self.fail("Passed PAYLOAD_FILE path does not exist.")
            try:
                with open(payload_file_path, 'r') as payload_file:
                    payload = json.loads(payload_file.read())
            except json.JSONDecodeError:
                self.fail(f'Failed to parse PAYLOAD_FILE {payload_file_path}: invalid JSON provided.')

        try:
            response = requests.post(
                url=webhook_url,
                headers=headers,
                json=payload,
                timeout=REQUESTS_DEFAULT_TIMEOUT
            )
        except requests.exceptions.Timeout as error:
            message = self.create_message(
                BASE_FAILED_MESSAGE,
                'Request to Slack timed out',
                error
            )
            self.fail(message)
        except requests.ConnectionError as error:
            message = self.create_message(
                BASE_FAILED_MESSAGE,
                'Connection Error',
                error
            )
            self.fail(message)

        self.log_info(f"HTTP Response: {response.text}")

        # https://api.slack.com/messaging/webhooks
        if 200 <= response.status_code <= 299:
            self.success(BASE_SUCCESS_MESSAGE)
        else:
            self.fail(BASE_FAILED_MESSAGE)

    def create_message(self, base_message, error_message, error_text):
        message = '{}: {}{}'.format(
            base_message,
            error_message,
            f': {error_text}' if self.get_variable("DEBUG") else f'.'
        )
        return message

    def run(self):
        super().run()

        if os.getenv('PAYLOAD_FILE'):
            self.log_info("Starting with payload provided in PAYLOAD_FILE...")
            self.schema.update(payload_file_schema)
        else:
            self.schema.update(attachments_schema)

        variables = self.validate()
        self.variables.update(variables)

        self.notify()


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = SlackNotifyPipe(schema=schema, pipe_metadata=metadata, check_for_newer_version=True)
    pipe.run()
